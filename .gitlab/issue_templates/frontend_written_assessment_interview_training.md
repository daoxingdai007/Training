# Frontend Written Assessment Interview Training 

<!--
## README FIRST

Title this issue with `Frontend Written Assessment Interview Training: [First Name] [Last Name]`. 
This issue should be remain confidential after creation and assigned to the person undergoing the training.
-->

Before reviewing the written assessment of frontend candidates, please complete all of the following training. 

1. [ ] Read through the [interview process](https://about.gitlab.com/handbook/hiring/interviewing/) in the handbook.
1. [ ] Read through the [frontend engineer](https://about.gitlab.com/job-families/engineering/frontend-engineer/) job family.
1. [ ] Read through the [written assessment questions](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md). Specifically note the process and examples of good/bad answers.
1. [ ] Watch the [written assessment training video](https://drive.google.com/a/gitlab.com/file/d/12Qh3em_kURiu_bXJhpS8pRt8huV3vxPX/view?usp=sharing)
1. [ ] Watch [training videos](https://drive.google.com/drive/u/0/folders/1IK3Wb3P9_u0akMx5ASG26cuehY_LeRe8) on how to use Greenhouse. You will only need to watch the "Interviewers" session.
1. [ ] Request your manager to add you to the [Assessment Reviewers list](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/AssessmentReviewers.md).

/label ~interviewing
/confidential
