## Becoming a GitLab Manager

<!--
## README FIRST

Title this issue with `Becoming a Manager: [Team Member Name]`. Assign this issue to the person training.
-->

Your first few months as a manager here at GitLab can be exhilarating.  It can also be challenging, especially when you need to quickly identify what is important for your success.

This new manager issue is a launchpad, and it connects you with crucial information about being a manager at GitLab.  It has been designed to ensure you start your journey as a manager with all the resources and training available.   

Items on this issue are identified as either required or suggested.  

  * **REQUIRED** items should be completed by all new GitLab managers.

  * *SUGGESTED* items are optional.  They may not be applicable to your style or team dynamic.  Experiment with them for a few days or weeks.  If you discover a useful practice or resources outside of this list, please submit a merge request to add it.

Since this checklist will evolve, please use the [commit history](https://gitlab.com/gitlab-com/people-group/Training/-/commits/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) to monitor changes.  Major changes will be communicated via the company call and slack.

Your contributions as a new manager are essential, submit a merge request to [this issue template](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) as ideas arise. 


### First Week

<details>
<summary>Click to expand/contract</summary>

#### Tasks
1. [ ] Team Member: Create a [retro :recycle: thread](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-development/#becoming-a-gitlab-manager-issue-retrospective) to capture any feedback and/or comments that you will collect while working on this issue. *SUGGESTED*
1. [ ] Schedule a handover meeting(s) with the current team manager.  Try the 1:1:1 format for a transparent handoff. ([Instructions](https://about.gitlab.com/handbook/leadership/1-1/#transitioning-1-1s), [Article](https://medium.com/making-meetup/the-manager-handoff-worksheet-c8acb2c899e6) & [Template](https://docs.google.com/document/d/16f_Nrf4rpy6P5SDXxXn7f5q9zEmhIZdjI1Fqvic7498/edit?usp=sharing)) **REQUIRED**
    1. **Items to review**
    - [ ] Most recent 360 review cycle feedback
    - [ ] More recent feedback, projects, and other info since the last review cycle
    - [ ] Professional goals and growth areas
    - [ ] Shared documents and other things for you to know
1. [ ] Schedule 1:1 meetings with team members ([Instructions](https://about.gitlab.com/handbook/leadership/1-1/) & [Template](https://about.gitlab.com/handbook/leadership/1-1/suggested-agenda-format/)) and complete the learning path on [Running an Effective 1:1 Meeting in GitLab Learn](https://gitlab.edcast.com/pathways/running-an-effective). **REQUIRED**
1. [ ] Write and share a [README](https://about.gitlab.com/handbook/leadership/#your-individual-readme) file or "How I like to work" document for your team. *SUGGESTED*
1. [ ] Create a weekly priorities thread ([Examples](https://gitlab.com/kencjohnston/README/-/blob/master/Priorities.md#2021-05-17)) so that the team knows what you will be focusing your time on *SUGGESTED*
1. [ ] Prompt the previous manager (if applicable) to update BambooHR to reflect the change in reporting structure per the [Job Information Change in BambooHR](https://about.gitlab.com/handbook/people-group/promotions-transfers/#job-information-change-in-bamboohr) directions. **REQUIRED**
    1. [ ] Review your BambooHR profile to confirm that you can see your direct reports. If you cannot after the previous manager has completed their request, please comment in the #people-connect Slack channel and they will ensure that you have `Manager` BambooHR level access. **REQUIRED**
1. [ ] Ensure that the [team page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page) is properly updated with your new reporting structure. Any organizational changes must be effective in BambooHR before they can be live on the team page.
**REQUIRED**
    1. [ ] Update your new direct report's [individual team page .yml file](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page) to ensure the `reports_to` field for your direct reports has your correct slug. Remember that you can edit any profile file by clicking on 'Edit in Web IDE' directly from an individual profile on the [team page](https://about.gitlab.com/company/team). *SUGGESTED*
    1. [ ] Update [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/stages.yml) to reflect the new structure of your Stage and Group (if applicable). 
    1. [ ] If you use business cards, [order new business cards](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards) with your new title.  
1. [ ] Be aware that as a manager, you will not have access to your direct report's personal information such as their address. If you ever need a direct report's personal information, for work related purposes only (including but not limited to [flower and gift purchasing](https://about.gitlab.com/handbook/people-group/celebrations/#flowers-and-gift-ordering)), then you must request this information from your direct report directly. Also, this information cannot be shared with other team members without explicit consent. 

#### Support Networks

1. [ ] Join the [#managers Slack channel](https://gitlab.slack.com/messages/managers/). **REQUIRED**
1. [ ] Join the [#eng-week-in-review Slack channel](https://gitlab.slack.com/messages/eng-week-in-review) **REQUIRED** for Engineering Division.
1. [ ] Join the monthly [Leadership Chats](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/) by accessing the team calendar and adding the event to your calendar. *SUGGESTED*
1. [ ] Submit an [access request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests) to be added to the `managers@gitlab.com` email group so you can attend the [Leadership Chats](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/). 
1. [ ] Consider Scheduling a coffee chat with the [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) who supports your Department.

#### Learning Modules

1. [ ] For GitLab team members moving into a managerial position: [Transitioning to a manager role](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-development/) **REQUIRED**
1. [ ] Read the [New to Leadership](https://about.gitlab.com/handbook/people-group/learning-and-development/new-to-leadership/) handbook page. **REQUIRED**
1. [ ] Complete the [Mitigating Risk Course](https://gitlab.edcast.com/pathways/mitigating-risk) within your first 60 days as a people leader
1. [ ] Review leadership resources in the [Leadership Handbook](https://about.gitlab.com/handbook/leadership) and spend time learning about: 
   1. [ ] [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/)
   1. [ ] Enhancing [emotional intelligence](https://about.gitlab.com/handbook/leadership/emotional-intelligence)
   1. [ ] [Career development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)
   1. [ ] Complete the [Giving Feedback Learning Pathway in GitLab Learn](https://gitlab.edcast.com/pathways/giving-feedback)
   1. [ ] Complete the [Managing Performance Learning Pathway in GitLab Learn](https://gitlab.edcast.com/pathways/feedback)
   1. [ ] Review [how to be a great remote manager](https://about.gitlab.com/company/culture/all-remote/being-a-great-remote-manager/) page in our Handbook *SUGGESTED*
   1. [ ] Read the [Understanding SOCIAL STYLES](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/) Handbook page.
   1. [ ] Watch and listen to [this video on "Neurodiversity in the Workplace"](https://youtu.be/KJwIDoxQeSc) by advocate Sophie Longley. A shared understanding allows us to have a bigger conversation about team members' unique working styles, to ensure we set up veryone for success, including those who may be undiagnosed or diagnosed neurodivergent but do not wish to be public with this.
   1. [ ] Learn how to [Build High Performing Teams](https://gitlab.edcast.com/pathways/building-high-performing-teams-this)
   1. [ ] Develop your skills on how to [run an effective 1:1 learning pathway in GitLab Learn](https://gitlab.edcast.com/pathways/running-an-effective) 
1. [ ] Book Recommendation - If you are interested in further developing skills on how to lead teams at GitLab, consider reading the book [High Output Management](https://about.gitlab.com/handbook/leadership/#building-high-performing-teams) which is a favorite by many of our leaders
1. [ ] Complete the [WILL Interactive Anti-Harassment Managers](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#compliance-courses) **REQUIRED** Upload your certificate to your Employee Documents folder in BambooHR once complete.
</details>
----

### Month 1

<details>
<summary>Click to expand/contract</summary>

#### Tasks

1. [ ] Review and discuss career development plans with each team member ([Instructions](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#what-is-career-development) & [Template](https://docs.google.com/document/d/1nBciVcCGXbeYKikufT_MZs1nn1iulKLOqXCgKaXDbQI/edit?usp=sharing)) **REQUIRED**
1. [ ] Meet with your leader and define or review measurable goals for your management role *SUGGESTED*
1. [ ] Try writing in a journal to reflect on your experiences as a new manager [(Instructions)](https://hbr.org/2016/01/want-to-be-an-outstanding-leader-keep-a-journal) *SUGGESTED*
1. [ ] Review the handbook on [management's role in PTO](https://about.gitlab.com/handbook/paid-time-off/#managements-role-in-paid-time-off) and learn how to pull reports from the PTO by Roots app.

#### Support Networks

1. [ ] Review [Team Member Resource Group](https://about.gitlab.com/company/culture/inclusion/erg-guide/) page for the benefit of yourself and your team members *SUGGESTED*

#### Learning Modules

1. [ ] [Career mapping at GitLab](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/) / [Video](https://www.youtube.com/watch?v=YoZH5Hhygc4) **REQUIRED**

     **Department Specific Paths**
      * [Engineering Career Development](https://about.gitlab.com/handbook/engineering/career-development/)
      * [Marketing Career Development](https://about.gitlab.com/handbook/marketing/career-development/)

1. [ ] [Hiring process at GitLab](https://about.gitlab.com/handbook/hiring/) **REQUIRED**
    1. [ ] Ensure you have completed Interview Training. Your issue should have already been automatically opened, and can be found in the [Training Project](https://gitlab.com/gitlab-com/people-group/Training/-/issues).
    1. [ ] [New hire probationary period process](https://about.gitlab.com/handbook/contracts/#probation-period)

1. [ ] As a GitLab Manager, you will play an important role in your new team member onboarding process. Review the [onboarding page](https://about.gitlab.com/handbook/people-group/general-onboarding/) and [your part to the new team members onboarding process](https://about.gitlab.com/handbook/people-group/general-onboarding/#managers-of-new-team-members).

1. [ ] As a GitLab Manager, you will need to be familiar with the current offboarding process. Review the [offboarding page](https://about.gitlab.com/handbook/people-group/offboarding/) in the Handbook as well as the current Manager tasks in the [offboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/offboarding.md). The offboarding tasks are time sensitive and the manager tasks need to be completed as soon as possible. 

1. [ ] Familiarize yourself on [compensation, benefits and equity at GitLab](https://about.gitlab.com/handbook/total-rewards/) as these topics will most likely come up in your conversations with your team members. If you are a new team member, these knowledge assessments are also part of your onboarding issue. *SUGGESTED*
    1. [ ] Take the [Compensation Knowledge Assessment quiz](https://about.gitlab.com/handbook/total-rewards/compensation/#knowledge-assessment)
    1. [ ] Take the [Benefits Knowledge Assessment quiz](https://about.gitlab.com/handbook/total-rewards/benefits/#knowledge-assessment)
    1. [ ] Take the [Equity Knowledge Assessment quiz](https://about.gitlab.com/handbook/stock-options/#equity-knowledge-assessment)
    1. [ ] Check-in with your team members to make sure they understand their Total Rewards package. For any questions around Compensation, Benefits or Equity, you can contact Total Rewards via total-rewards@gitlab.com.

#### Handbook Review

1. [ ] [List of recommended leadership books](https://about.gitlab.com/handbook/leadership/#books) *SUGGESTED*
1. [ ] Familiarize yourself with the [Expense reimbursement policy](https://about.gitlab.com/handbook/finance/expenses/#-expense-reimbursement) and [how to approve expense reports](https://about.gitlab.com/handbook/finance/expenses/#-approving-expense-reports)


</details>


----
### Month 2
<details>
<summary>Click to expand/contract</summary>

#### Tasks

1. [ ] Schedule a coffee chat with [the recruiter(s)](https://about.gitlab.com/handbook/hiring/recruiting-alignment/) who supports your Department. *SUGGESTED* 
1. [ ] Discuss and practice the following topics with your mentor(s) *SUGGESTED*
    * Performance feedback
    * 360 Survey feedback meetings

#### Learning Modules
1. [ ] [Giving Performance Feedback](https://about.gitlab.com/handbook/leadership/#giving-feedback) **REQUIRED**
1. [ ] [360 Survey Feedback Training](https://about.gitlab.com/handbook/people-group/360-feedback/#360-feedback) **REQUIRED**
1. [ ] [360 Feedback Meetings](https://about.gitlab.com/handbook/people-group/360-feedback/#360-feedback-meeting) **REQUIRED**
1. [ ] [Decision Making at GitLab](https://about.gitlab.com/handbook/leadership/making-decisions/) **REQUIRED**
1. [ ] [Underperformance](https://about.gitlab.com/handbook/leadership/underperformance/) **REQUIRED**
1. [ ] Sourcing at GitLab *SUGGESTED*
    1. [ ] Becoming a [GitLab Ambassador](https://about.gitlab.com/handbook/hiring/gitlab-ambassadors/)
    1. [ ] [Upgrading Your LinkedIn Account for Sourcing](https://about.gitlab.com/handbook/hiring/sourcing/#upgrading-your-linkedin-account) 
    1. [ ] [How to use Linkedin Recruiter](https://business.linkedin.com/talent-solutions/cx/18/05/fast-track#all)
    1. [ ] [Linkedin Training presentation](https://docs.google.com/presentation/d/1W9PvVp2uGFsWHFTQrAd5ZjgzfAMmS_dQI6R7Lg7XDJc/edit#slide=id.g7ba34d75e8_0_16) 
    1. [ ] [Sourcing Guide](https://docs.google.com/presentation/d/1M03qxZn9hy5pdeafvGabrQEeovtfvby7Vyc-FC620ts/edit?ts=5eba016a#slide=id.g29a70c6c35_0_68)
    1. [ ] [Sourcing handbook page](https://about.gitlab.com/handbook/hiring/sourcing/) 

</details>

### Month 3

<details>
<summary>Click to expand/contract</summary>

#### Tasks

1. [ ] Discuss and practice the following topics with your mentor(s) *SUGGESTED*
    * Time management techniques
    * Decision making strategies


#### Learning Modules
1. [ ] [Promotion process at GitLab](https://about.gitlab.com/handbook/people-group/promotions-transfers/) **REQUIRED**
1. [ ] [Job family creation](https://about.gitlab.com/handbook/hiring/job-families/#new-job-family-creation) **REQUIRED**
1. [ ] Sign up for the next [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/) to further develop your skills as a Manager

</details>

----
### Ongoing

<details>
<summary>Click to expand/contract</summary>

#### Support Networks
1. [ ] [Leadership Chats](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/)
1. [ ] [#managers Slack channel](https://gitlab.slack.com/messages/managers/)
1. [ ] [#people-connect Slack channel](https://gitlab.slack.com/archives/C02360SQQFR)

#### Learning Module
**Every two years**
* [WILL Interactive Anti-Harassment Managers](/handbook/people-group/learning-and-development/compliance-courses/#compliance-courses) **REQUIRED**

#### Reference
* [Tips from GitLab Management Group](https://about.gitlab.com/handbook/leadership/#management-group)
* [Engineering Management](https://about.gitlab.com/handbook/engineering/management)


</details>

----
### Admin Tasks (People Group, L&D, etc)

<details>
<summary>Click to expand/contract</summary>

#### Learning & Development
1. [ ] @jallen16 Confirm new manager has completed the correct level of anti-harassment training, if not send a new invite via WILL Learning  


</details>

/label ~"Manager Training"
