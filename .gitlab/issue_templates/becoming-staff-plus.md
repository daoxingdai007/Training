## Becoming Staff+ at Gitlab

<!--
## README FIRST

Title this issue with `Becoming Staff+: [Team Member Name]`. Assign this issue to the person training.
-->

Your first few months as a Staff+ Engineer here at GitLab can be exhilarating.  It can also be challenging, especially when you need to quickly identify what is important for your success.

This becomimg a Staff+  issue is a launchpad, and it connects you with crucial information about being a Staff+ Engineer at GitLab.  It has been designed to ensure you start your journey as a Staff+ Engineer with all the resources and training available.   

Items on this issue are identified as either required or suggested.  

  * **Required** items should be completed by all new GitLab Staff+ Engineers.

  * **Suggested** items are optional.  They may not be applicable to your style or team dynamic.  Experiment with them for a few days or weeks.  If you discover a useful practice or resources outside of this list, please submit a merge request to add it.

Since this checklist will evolve, please use the [commit history](https://gitlab.com/gitlab-com/people-group/Training/-/commits/master/.gitlab/issue_templates/becoming-staff-plus.md) to monitor changes.  Major changes will be communicated via the company call and slack.

Your contributions as a new Staff+ Engineer are essential, submit a merge request as ideas arise. If you have any questions about items on this checklist, please comment on this [issue](https://gitlab.com/gitlab-com/people-group/Training/-/issues/27).


### First Week

<details>
<summary>Click to expand/contract</summary>

#### Tasks
1. [ ] Team Member: Create a retro :recycle: thread to capture any feedback and/or comments that you will collect while working on this issue. **Suggested**

1. [ ] Write and share a README file or "How I like to work" document for your team ([Examples](https://about.gitlab.com/handbook/engineering/readmes/)) *SUGGESTED*
1. [ ] Ensure that the [team page](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#updating-the-team-page-and-org-chart) is properly updated with your new title
**REQUIRED**
    1. [ ] Prompt your manager to update BambooHR to reflect the change in your title
    1. [ ] [Order new business cards](https://about.gitlab.com/handbook/people-group/frequent-requests/#business-cards) with your new title 


#### Support Networks

1. [ ] Join the [#managers Slack channel](https://gitlab.slack.com/messages/managers/) **SUGGESTED**
1. [ ] Join the [#eng-week-in-review Slack channel](https://gitlab.slack.com/messages/eng-week-in-review) **REQUIRED**
1. [ ] Join the [Leadership Chats](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/) by accessing the team calendar and adding the event to your calendar. The chats run monthly and more information can be found on the [page](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/). *SUGGESTED*


#### Learning Modules

1. [ ] Review leadership resources in the [Leadership Handbook](https://about.gitlab.com/handbook/leadership) and spend time learning about: 
   1. [ ] [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/)
   1. [ ] Enhancing [emotional intelligence](https://about.gitlab.com/handbook/leadership/emotional-intelligence)
   1. [ ] Complete the [Giving Feedback Learning Pathway in GitLab Learn](https://gitlab.edcast.com/pathways/giving-feedback)
   1. [ ] Read the [Understanding SOCIAL STYLES](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/) Handbook page.
   1. [ ] Watch and listen to [this video on "Neurodiversity in the Workplace"](https://youtu.be/KJwIDoxQeSc) by advocate Sophie Longley. A shared understanding allows us to have a bigger conversation about team members' unique working styles, to ensure we set up veryone for success, including those who may be undiagnosed or diagnosed neurodivergent but do not wish to be public with this.
   1. [ ] Learn how to [Build High Performing Teams](https://gitlab.edcast.com/pathways/building-high-performing-teams-this)
</details>
----

### Month 1

<details>
<summary>Click to expand/contract</summary>

#### Tasks

1. [ ] Meet with your manager and define or review measurable goals for your new role *REQUIRED*
1. [ ] Try writing in a journal to reflect on your experiences as a new manager [(Instructions)](https://hbr.org/2016/01/want-to-be-an-outstanding-leader-keep-a-journal) *SUGGESTED*

#### Support Networks

1. [ ] Review [Team Member Resource Group](https://about.gitlab.com/company/culture/inclusion/erg-guide/) page for the benefit of yourself and your team members *SUGGESTED*

#### Learning Modules

TBD
1. [ ] The [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/) at GitLab providers training geared towards managers, but is primarily focused on leadership and working with a diverse team. Consider participating. *SUGGESTED*

 #### Handbook Review

1. [ ] [List of recommended leadership books](https://about.gitlab.com/handbook/leadership/#books) *SUGGESTED*


</details>


----
### Month 2
<details>
<summary>Click to expand/contract</summary>

#### Tasks

1. [ ] Discuss and practice the following topics with your mentor(s) *SUGGESTED*
    * Performance feedback
    * 360 Survey feedback meetings
2. [ ] If you don't yet have a mentor, start by reviewing the [available mentors at GitLab](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/#find-a-mentor) and reach out to see if they are available.
#### Learning Modules
1. [ ] [Giving Performance Feedback](https://about.gitlab.com/handbook/leadership/#giving-feedback) **REQUIRED**
1. [ ] [360 Survey Feedback Training](https://about.gitlab.com/handbook/people-group/360-feedback/#360-feedback) **REQUIRED**
1. [ ] [Decision Making at GitLab](https://about.gitlab.com/handbook/leadership/#sts=Making%20Decisions) **REQUIRED**

</details>

### Month 3

<details>
<summary>Click to expand/contract</summary>

#### Tasks

1. [ ] Discuss and practice the following topics with your mentor(s) *SUGGESTED*
    * Time management techniques
    * Decision making strategies


#### Learning Modules

1. [ ] [How to set goals when everything feels like a priority](https://gitlab.edcast.com/insights/ECL-494fb25d-efc8-4ca9-ab26-0d820e02bcaa)
1. [ ] [Manage your energy](https://gitlab.edcast.com/insights/ECL-2c264301-2f5f-408d-99d2-c59a5225d7e4)

</details>

----
### Ongoing

<details>
<summary>Click to expand/contract</summary>

#### Support Networks
1. [ ] [Leadership Chats](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/leadership-chats/)
1. [ ] [#peopleops Slack channel](https://gitlab.slack.com/messages/peopleops/)

#### Learning Module
**Every two years**
* [WILL Interactive Anti-Harassment Managers](/handbook/people-group/learning-and-development/compliance-courses/#compliance-courses) **REQUIRED**

#### Reference


</details>

----
### Admin Tasks (People Group, L&D, etc)

<details>
<summary>Click to expand/contract</summary>


</details>
