## Interview Training

<!--
## README FIRST

1. Title this issue with `Interview Training: [Team Member Name]`.
2. This issue should be remain confidential after creation, as the STAR interviewing training video linked below should be kept internal to GitLab as it contains confidential information.
3. Assign this issue to the person training.
4. Add Labels for the appropriate Division and Department the person in training is a member of.
-->

Before engaging candidates in a screening call or interview, please complete all of the following training.
**Note**: The videos linked are on [GitLab Unfiltered Youtube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) and will not be available to you unless **you are [logged into](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) the Unfiltered Youtube account**.  

1. [ ] Read the [interview process](https://about.gitlab.com/handbook/hiring/interviewing/) and [conducting an interview](https://about.gitlab.com/handbook/hiring/conducting-a-gitlab-interview/) in the handbook.
   1. [ ] Please confirm you understand that we do not ask for or record someone's pay history information. We only ask for and record *salary expectations*. Additionally, the candidate's salary expectations should only be stored in the Candidate's `Private Notes` tab in Greenhouse. Only recruiters should be talking about compensation information with candidates. 
1. [ ] Familiarize yourself with the process for each role you'll be interviewing in the [`hiring-process` repository](https://gitlab.com/gitlab-com/people-group/hiring-processes)
1. [ ] Watch these training videos: 
   1. [ ] [Preparing for an interview](https://www.youtube.com/watch?v=k9qQ9uEiLvs) 
   1. [ ] [During the interview](https://www.youtube.com/watch?v=2POya3xr6fo) or read the [deck](https://docs.google.com/presentation/d/137QTrBBvM3Lfo_eO6yRSrWfZRJ_UWrRuvxzc1xwTIMg/edit?usp=sharing).
   1. [ ] [Behavioural/STAR interview](https://www.youtube.com/watch?v=cLus0qUfhro) or read the [deck](https://docs.google.com/presentation/d/1yYITcmXxrysO9RsI-tUgbmhVZW5Pb8IVaShBFJbWO2c/edit?usp=sharing).
   1. [ ] [Illegal interview questions](https://www.youtube.com/watch?v=6GpJ2K8l3uE) or read the [deck](https://docs.google.com/presentation/d/1QSvFI3PZ8NNnphzHzL5vOwTxuG54XZIiqvuz2OIbHQY/edit#slide=id.g7565b8f9f1_0_449).
1. [ ] Complete all modules of [Uncovering Unconscious Bias in Recruiting and Interviewing](https://gitlab.edcast.com/insights/ECL-26954271-f4a7-4fbc-a6aa-291dfdef9f57).  
   1. [ ] Once you've completed the modules and passed the exam download the LinkedIn Learning certificate and attach the PDF in a comment at the bottom of this issue.
1. [ ] Review our [D&I Training and Learning Opportunities](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion--belonging-training-and-learning-opportunities) to learn more about how to be more inclusive. 
1. [ ] Watch this [training video](https://youtu.be/ca-FeCZMDto) on how to use Greenhouse as an Interviewer or Hiring Manager 
1. [ ] Read the Greenhouse [handbook page](https://about.gitlab.com/handbook/hiring/greenhouse/). 
1. [ ] Ensure your [office hours](https://support.google.com/calendar/answer/7638168?hl=en) are set up in your Google calendar.
1. [ ] Log into your [Prelude](https://prelude.co/) account using your GitLab Google account to update your profile, which includes your office hours and interview preferences.
1. [ ] Ensure that your [LinkedIn profile is up-to-date](https://about.gitlab.com/handbook/hiring/gitlab-ambassadors/#1-optimize-your-social-media-profiles-especially-linkedin). This along with your Slack photo and team page entry will be shared with candidates who reach the team interview stage.
1. [ ] Login on [app.guide.co](https://app.guide.co/) using your GitLab Google account to edit your profile and upload a photo. Guide is a [tool](https://about.gitlab.com/handbook/hiring/guide/) that helps our hiring team manage candidate experience.
1. [ ] Reach out to a member on your team to shadow at least two interviews. 
    * Remind the team member you are shadowing to email the candidate mentioning they will be shadowed by another GitLab team member and ask if the candidate is comfortable with that. Your team member should be able to find the candidate's email in Greenhouse. If the team member is not comfortable sending email, they may ask one of the Recruiting team members to send it, but note that it should be done ~2 days in advance. As an alternative, the team member may also ask whether the candidate is comfortable with shadowing at the very beginning of the interview before you join. 
    * We encourage the shadowing team member to collect feedback in addition to the interviewer during these sessions, so you can get used to taking notes that are impactful and relevant. 
    * Remember to note that you are a shadow interviewer in the scorecard's Overall Recommendation: Key Take-Aways input before submission. This is so anyone reviewing your feedback is aware of your training status. 
    1. [ ] First Shadow Scorecard Submitted <!--Being an observer to the interview these notes could also be used to provide feedback to the interviewer as well as contribute to the hiring discussion of the candidate.  This should be provided to the interviewer for inclusion in the candidate's profile in Greenhouse. <I don't understand what this content is trying to get at - Justin Mandell -->
    1. [ ] Second Shadow Scorecard Submitted 
1. [ ] Lead an interview, with a qualified interviewer shadowing you. Get feedback from the shadow after the interview. 
    * You are welcome to repeat this step until you are comfortable with leading.
1. [ ] Set up a personal reminder to be shadowed by an experienced interviewer after 3 months of interviewing.  This is essential to help us maintain high standards in our interviewing process. 

## Job-specific tasks

### Product Designers

1. [ ] At the beginning of the training, add yourself to the [Product Designer Interview Shadowing](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/UX/Interview-shadowing.md) table. Remember to update the table after you shadow/lead any interviews. In order to complete training, you'll need to complete 2 interview shadows and 2 lead interviews with a qualified interview shadowing you.
1. [ ] Once you complete this training, remove yourself from the Interview Shadowing table and add your name to the [Product Designer Interviewer Pool](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/UX/Product-Designer/Interviewers.md).

### Support Engineers

1. [ ] After every shadow interview, make sure to update [Interviewers with open interview trainings (tag in to shadow)](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) part in the interviews section. For better scheduling of support shadows for an interview.

/label ~"Interview Training"
/confidential
